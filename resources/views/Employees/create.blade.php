@extends('master')
@section('main-content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add Employees</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Add Employees</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  
<section class="content">

<div class="form-group ">
<form action="{{route('employees.store')}}" method="POST"  enctype="multipart/form-data">
{{csrf_field()}}
<div class="row">
<div class="col-md-5">
  <label class="control-label">
   Name
  </label>
  <br>
  <div class="input-group">
   <input class="form-control" id="name" name="name" type="text" placeholder="Your name"/>
   <span class="text-danger">{{ $errors->first('name') }}</span>
  </div>
</div>
<div class="col-md-5">
  <label class="control-label">
   Email
  </label>
  <br>
  <div class="input-group">
   <input class="form-control" id="email" name="email" type="text" placeholder="Your Email"/>
   <span class="text-danger">{{ $errors->first('email') }}</span>
  </div>
  </div>
 </div> 
 <div class="col-md-10">
  <label class="control-label">
   Address
  </label>
  <br>
  <div class="input-group">
   <input class="form-control" id="address" name="address" type="text" placeholder="Your Address"/>
   <span class="text-danger">{{ $errors->first('address') }}</span>
  </div>
  <div class="row">
  <div class="col-md-5">
  <label class="control-label">
   Contact
  </label>
  <br>
  <div class="input-group">
   <input class="form-control" id="contact" name="contact" type="text" placeholder="Your Contact"/>
   <span class="text-danger">{{ $errors->first('contact') }}</span>
  </div>
  </div>
  <div class="col-md-5">
  <label class="control-label">
   CNIC
  </label>
  <br>
  <div class="input-group">
   <input class="form-control" id="cnic" name="cnic" type="text" placeholder="35202-1000000-2"/>
   <span class="text-danger">{{ $errors->first('cnic')}}</span>
  </div>
  </div>
  </div>
  <div class="row">
  <div class="col-md-5">
  <br>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Designation</label>
    <select class="form-control" id="exampleFormControlSelect1" name="designation">
    <span class="text-danger">{{ $errors->first('designation')}}</span>
      <option value="supervisor">Supervisor</option>
      <option value="manager">Manager</option>
      <option value="operator">Operator</option>
      <option value="engineer">Engineer</option>
      <option value="HR">HR</option>
    </select>
  </div>
  </div>
  </div>
  <label class="control-label">Upload Photo</label>
  <br>
  <input type="file" name="image" id="image">
  <span class="text-danger">{{ $errors->first('image') }}</span>
    <div class="col-md-5">
    <br>
      <button type="submit" class="btn btn-success">Save</button>
    </div>
  </div>
  </form>
 </div>
 </section>
@endsection