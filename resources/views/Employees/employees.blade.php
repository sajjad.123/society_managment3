@extends('master')
@section('main-content')

<div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Employees Data</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Employees Data</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
    <div class="col-md-12">
@if(session()->get('success'))
 <div class="alert alert-success">
 {{session()->get('success')}}
 @endif
 @if(session()->get('danger'))
 <div class="alert alert-danger">
 {{session()->get('danger')}}
 @endif
    </div>
</div>
<section class="content">
<div class="container-fluid">
<div class="row">
    <table class="table table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Contact</th>
      <th scope="col">Address</th>
      <th scope="col">CNIC</th>
      <th scope="col">Designation</th>
      <th scope="col">Image</th>
      <th colspan="2">Action</th>
    </tr>
  </thead>
  <tbody>

  @foreach($employees as $key => $employee)
              <tr>
                 <td>{{ $key+1 }}</td>
                 <td>{{ $employee->name }}</td>
                 <td>{{ $employee->email }}</td>
                 <td>{{ $employee->contact }}</td>
                 <td>{{ $employee->address }}</td>
                 <td>{{ $employee->cnic }}</td>
                 <td>{{ $employee->designation }}</td>
                 <td><img src="{{asset('picture/' .$employee->image)}}" class="thumbnail" width="50"></td>
                 <td><a href="{{ route('employees.edit',$employee->id)}}" class="btn btn-success">Edit</a></td>
                 <td>
                 <form action="{{ route('employees.destroy', $employee->id)}}" method="post">
                 {{ csrf_field() }}
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
                </td>
              </tr>
              @endforeach
    </tbody>
    </table>
   </div>
   </div>
   </section>
 @endsection
  
