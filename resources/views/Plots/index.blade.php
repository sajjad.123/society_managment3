@extends('master')
@section('main-content')

<div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Manage Plots Data</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Manage Plots Data</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
@if(session()->get('success'))
 <div class="alert alert-success">
 {{session()->get('success')}}
 @endif
 @if(session()->get('danger'))
 <div class="alert alert-danger">
 {{session()->get('danger')}}
 @endif
    </div>
</div>
<section class="content">
<div class="container-fluid">
<div class="row">
    <table class="table table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Plot No</th>
      <th scope="col">Plot Size</th>
      <th scope="col">Plot Unit</th>
      <th scope="col">Plot Type</th>
      <th scope="col">Street</th>
      <th scope="col">Sector</th>
      <th scope="col">Price</th>
      <th colspan="2">Action</th>
    </tr>
  </thead>
  <tbody>

  @foreach($plot as $key => $plott)
              <tr>
                 <td>{{ $key+1 }}</td>
                 <td>{{ $plott->plotno }}</td>
                 <td>{{ $plott->plotsize }}</td>
                 <td>{{ $plott->plotunit }}</td>
                 <td>{{ $plott->type }}</td>
                 <td>{{ $plott->streetno }}</td>
                 <td>{{ $plott->sector }}</td>
                 <td>{{ $plott->price }}</td>
                 <td><a href="{{ route('plot.edit',$plott->id)}}" class="btn btn-success">Edit</a></td>
                 <td>
                 <form action="{{ route('plot.destroy', $plott->id)}}" method="post">
                 {{ csrf_field() }}
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
                </td>
              </tr>
              @endforeach
    </tbody>
    </table>
   </div>
   </div>
   </section>
 @endsection
  
