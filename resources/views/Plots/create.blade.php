@extends('master')
@section('main-content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add Plot</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Add Plot</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  
<section class="content">

<div class="form-group ">
<form action="{{route('plot.store')}}" method="POST"  enctype="multipart/form-data">
{{csrf_field()}}
<div class="row">
<div class="col-md-10">
  <label class="control-label">
   Plot Number
  </label>
  <br>
  <div class="input-group">
   <input class="form-control" id="plotno" name="plotno" type="text" placeholder="Plot Number"/>
   <span class="text-danger">{{ $errors->first('plotno') }}</span>
  </div>
</div>
 </div>
 <br> 
 <div class="row">
 <div class="col-md-5">
  <label class="control-label">
   Plot Size<b>(Dimensions)</b>
  </label>
  <br>
  <div class="input-group">
   <input class="form-control" id="plotsize" name="plotsize" type="text" placeholder="3*4"/>
   <span class="text-danger">{{ $errors->first('plotsize') }}</span>
  </div>
  </div>
  <div class="col-md-5">
  <label class="control-label">
   Plot Size(Unit)
  </label>
  <br>
  <div class="input-group">
   <input class="form-control" id="plotunit" name="plotunit" type="text" placeholder=" Marla"/>
   <span class="text-danger">{{ $errors->first('plotunit') }}</span>
  </div>
  </div>
  </div>
  <div class="row">
  <div class="col-md-10">
  <br>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Type</label>
    <select class="form-control" id="exampleFormControlSelect1" name="type">
    <span class="text-danger">{{ $errors->first('type')}}</span>
      <option value="commercial">Commerical</option>
      <option value="residential">Residential</option>
      <option value="govermental">Govermental</option>
      <option value="society">Society</option>
    </select>
  </div>
  </div>
  </div>
  <div class="row">
 <div class="col-md-5">
  <label class="control-label">
   Street Number
  </label>
  <br>
  <div class="input-group">
   <input class="form-control" id="streetno" name="streetno" type="text" placeholder="Stree #"/>
   <span class="text-danger">{{ $errors->first('streetno') }}</span>
  </div>
  </div>
  <div class="col-md-5">
  <label class="control-label">
   Sector
  </label>
  <br>
  <div class="input-group">
   <input class="form-control" id="sector" name="sector" type="text" placeholder="Sector"/>
   <span class="text-danger">{{ $errors->first('sector') }}</span>
  </div>
  </div>
  </div>
  <br>
  <div class="row">
<div class="col-md-10">
  <label class="control-label">
   Price
  </label>
  <br>
  <div class="input-group">
   <input class="form-control" id="price" name="price" type="text" placeholder="Enter Price"/>
   <span class="text-danger">{{ $errors->first('price') }}</span>
  </div>
</div>
 </div>
    <div class="col-md-5">
    <br>
      <button type="submit" class="btn btn-success">Add Plot</button>
    </div>
  </div>
  </form>
 </div>
 </section>
@endsection