<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $fillable=[
        'name','email','address','contact','cnic','designation','image',
    ];
}
