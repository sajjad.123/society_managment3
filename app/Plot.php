<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plot extends Model
{
    protected $fillable=[
         'plotno','plotsize','plotunit','type','streetno','sector','price',
    ];
}
