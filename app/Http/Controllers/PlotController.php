<?php

namespace App\Http\Controllers;

use App\Plot;
use Illuminate\Http\Request;
use Redirect;
class PlotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plot = Plot::all();
        return view('Plots.index',['plot' => $plot]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $plot=new Plot();
        $request->validate([
        'plotno' => 'required',
        'plotsize' => 'required',
        'plotunit' => 'required',
        'type' => 'required',
        'streetno' => 'required',
        'sector' => 'required',
        'price' => 'required',
    ]);
    
    $plot->plotno =$request->input('plotno');
    $plot->plotsize =$request->input('plotsize');
    $plot->plotunit =$request->input('plotunit');
    $plot->type =$request->input('type');
    $plot->streetno =$request->input('streetno');
    $plot->sector =$request->input('sector');
    $plot->price =$request->input('price');
    $plot->save();
    return redirect('/plot')->with('success','Successfully Added',$plot);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Plot  $plot
     * @return \Illuminate\Http\Response
     */
    public function show(Plot $plot)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Plot  $plot
     * @return \Illuminate\Http\Response
     */
    public function edit(Plot $plot)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plot  $plot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Plot $plot)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Plot  $plot
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Plot::where('id',$id)->delete();
        return Redirect::to('plot')->with('danger','Plot deleted successfully');
    }
}
