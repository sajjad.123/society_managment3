<?php

namespace App\Http\Controllers;

use App\Employees;
use Illuminate\Http\Request;
use Redirect;
use DB;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employees::all();
        return view('Employees.employees',['employees' => $employees]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('Employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employees=new Employees();
        $request->validate([
        'name' => 'required',
        'email' => 'required',
        'contact' => 'required',
        'address' => 'required',
        'cnic' => 'required',
        'designation' => 'required',
        'image' => 'required',
    ]);
    
    $employees->name =$request->input('name');
    $employees->email =$request->input('email');
    $employees->contact =$request->input('contact');
    $employees->address =$request->input('address');
    $employees->cnic =$request->input('cnic');
    $employees->designation =$request->input('designation');

    if($request->hasfile('image')){
        $file = $request->file('image');
        $extension = $file->getClientOriginalExtension();
        $filename = time(). '.'  .$extension;
        $file->move('picture/',$filename);
        $employees->image=$filename;
     }
    else{
        return $request;
        $employees->image='';
  }
  $employees->save();
    return Redirect::to('/employees')
   ->with('success','Greate! Data created successfully.',$employees);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function show(Employees $employees)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,$id)
    {
           $where = array('id' => $id);
           $data['employee'] = Employees::where($where)->first();
           return view('Employees.edit', $data);
        // $employee = Employees::findOrFail($id);

        // return view('Employees.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Employees $employees)
    {
        $employees=new Employees();
        $request->validate([
        'name' => 'required',
        'email' => 'required',
        'contact' => 'required',
        'address' => 'required',
        'cnic' => 'required',
        'designation' => 'required',
        'image' => 'required',
    ]);
    
    $employees->name =$request->input('name');
    $employees->email =$request->input('email');
    $employees->contact =$request->input('contact');
    $employees->address =$request->input('address');
    $employees->cnic =$request->input('cnic');
    $employees->designation =$request->input('designation');

    if($request->hasfile('image')){
        $file = $request->file('image');
        $extension = $file->getClientOriginalExtension();
        $filename = time(). '.'  .$extension;
        $file->move('picture/',$filename);
        $employees->image=$filename;
     }
    else{
        return $request;
        $employees->image='';
  }
  $employees->save();
    return Redirect::to('/employees')
   ->with('success','Greate! Employee Data updated successfully.',$employees);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employees::where('id',$id)->delete();
        return Redirect::to('employees')->with('danger','Employees Data deleted successfully');
    }
}
