<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('Welcome');
});


Route::get('/home', 'HomeController@index')->name('home');
//Admin Panel login 
Route::group(['middleware' => ['auth','admin']],function(){
    Route::get('/admin', function () {
        return view('Layout.dashboard');
    });
    
    //Employees Route and View
         Route::get('/create',function(){
            return view('Employees.create');
        });
        Route::get('/employees',function(){
        return view('Employees.employees');
        });
        
        Route::get('/addplot',function(){
            return view ('Plots.create');
         });
         Route::get('/plot',function(){
            return view ('Plots.index');
         });
         Route::get('/search','EmployeesController@search');
});
//Employee Resource Controller
Route::resource('employees','EmployeesController');
Route::resource('plot','PlotController');

Auth::routes(['verify' => true]);

